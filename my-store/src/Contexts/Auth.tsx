import React, { createContext, useState } from "react";
import { cloneDeep } from "lodash";

import {
  LoginInterface,
  login as authLogin,
  logout as authLogout,
} from "../services/AuthService";

interface LoginState {
  loggedIn: boolean;
  logginIn: boolean;
}

interface AuthProps {
  loggedInState: LoginState;
  login: (userName: string, password: string) => Promise<LoginInterface>;
  logout: () => void;
}

const defaultProps: AuthProps = {
  loggedInState: {
    loggedIn: false,
    logginIn: false,
  },
  login: () => {},
  logout: () => {},
};

export const AuthContext = createContext<AuthProps>(defaultProps);

interface Props {
  children: React.ReactNode;
}

export const AuthProvider: React.FC<Props> = ({ children }) => {
  const [loginState, setLoginState] = useState<LoginState>({
    loggedIn: false,
    logginIn: false,
  });

  const login = async (
    username: string,
    password: string
  ): Promise<LoginInterface> => {
    const returnData: LoginInterface = {
      success: false,
    };
    setLoginState({ ...loginState, logginIn: true });
    const newLoginState = cloneDeep(loginState);
    try {
      const loggedInCall = await authLogin(username, password);
      if (loggedInCall.success) {
        newLoginState.loggedIn = true;
        returnData.success = true;
      } else {
        newLoginState.loggedIn = true;
        returnData.error = {
          headline: "Problem",
          text: "Problem",
        };
      }
    } catch (e: any) {
      console.log(e);
      newLoginState.loggedIn = true;
      returnData.error = {
        headline: "Problem",
        text: "Problem",
      };
    }
    console.log(newLoginState);
    newLoginState.logginIn = false;
    setLoginState(newLoginState);
    return returnData;
  };

  const logout = async () => {
    const newLoginState = cloneDeep(loginState);
    newLoginState.logginIn = false;
    try {
    } catch (e: any) {
      console.log(e);
    }
    setLoginState(newLoginState);
  };

  return (
    <AuthContext.Provider
      value={{
        loggedInState: loginState,
        login,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
