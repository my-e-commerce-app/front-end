import { useState, useContext } from "react";
import TextButton from "../Components/TextButton";

import TextElement from "../Components/TextElement";
import TextField from "../Components/TextField";
import { AuthContext } from "../Contexts/Auth";

export const Login: React.FC = () => {
  const { login, loggedInState } = useContext(AuthContext);
  const { logginIn } = loggedInState;

  const [username, setUsername] = useState("");
  const [usernameValid, setUsernameValid] = useState(false);
  const [password, setPassword] = useState("");
  const [passwordValid, setPasswordValid] = useState(false);

  const handleUsernameChange = (name: string) => {
    setUsername(name);
    setUsernameValid(name.length > 0);
  };

  const handlePasswordChange = (password: string) => {
    setPassword(password);
    setPasswordValid(password.length > 0);
  };

  const handleSubmit = async () => {
    try {
      console.log("Her");
      const responseData = await login(username, password);
      console.log(responseData);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="bg-purple-300 w-screen h-screen flex flex-col justify-center items-center gap-10">
      <TextElement
        style="bold"
        size="large"
        value="Log in to see your daily items for slaying dragons !"
      />
      <div className="flex flex-row items-start gap-10">
        <form
          className="bg-slate-100 w-56 h-56 rounded-lg m-2 p-2 gap-2 flex-col flex pt-5"
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          <TextField
            placeholder="Username"
            value={username}
            state={usernameValid}
            onChange={(e) => {
              handleUsernameChange(e.target.value);
            }}
          />
          <TextField
            placeholder="Password"
            value={password}
            state={passwordValid}
            onChange={(e) => {
              handlePasswordChange(e.target.value);
            }}
            type="password"
          />
          <TextButton
            loading={logginIn}
            text="Get your gear!"
            color="blue"
            className="mt-auto"
            disabled={(!passwordValid && !usernameValid) || logginIn}
            onClick={handleSubmit}
          />
        </form>
        <img src="/images/swordsman.png" width={250} height={300} />
      </div>
    </div>
  );
};
