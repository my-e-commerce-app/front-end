const References: React.FC = () => {
  return (
    <li>
      <a href="https://www.flaticon.com/free-icons/dragon" title="dragon icons">
        Dragon icons created by Icongeek26 - Flaticon
      </a>
      <a
        href="https://www.flaticon.com/free-icons/warrior"
        title="warrior icons"
      >
        Warrior icons created by max.icons - Flaticon
      </a>
      <a href="https://www.flaticon.com/free-icons/wagon" title="wagon icons">
        Wagon icons created by Futuer - Flaticon
      </a>
      <a href="https://www.flaticon.com/free-icons/rpg" title="rpg icons">
        Rpg icons created by max.icons - Flaticon
      </a>
    </li>
  );
};
