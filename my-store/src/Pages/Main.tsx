import { NavigationBar } from "../modules/NavigationBar";

export const Main: React.FC = () => {
  return (
    <div className="bg-purple-300 w-screen h-screen flex flex-col">
      <NavigationBar />
    </div>
  );
};
