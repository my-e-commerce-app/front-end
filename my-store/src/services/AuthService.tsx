import axios from "axios";
import { AxiosError } from "axios";

import { ErrorInterface } from "../interfaces/interfaces";
import authHeader from "./AuthHeader";

const API_URL = "http://localhost:8000/";

export interface LoginInterface {
  success: boolean;
  error?: ErrorInterface;
}

export const login = async (
  user_name: string,
  password: string
): Promise<LoginInterface> => {
  const loginData: LoginInterface = {
    success: false,
    error: {
      headline: "Unkown fault",
      text: "Unkown",
    },
  };

  try {
    const response = await axios.post(API_URL + "auth/login", {
      user_name,
      password,
    });
    if (response.data.access) {
      localStorage.setItem("accessToken", response.data.access);
      localStorage.setItem("refreshToken", response.data.refresh);
      loginData.success = true;
      return loginData;
    }
    loginData.error = {
      headline: "Failed to login",
      text: "Failed to login",
    };

    return loginData;
  } catch (e: any) {
    const axiosError: AxiosError = e;
    console.log(axiosError);
    if (axiosError.response) {
      loginData.error = {
        headline: "Failed to login",
        text: "Failed to login, make sure user and / or password is correct",
      };
    } else {
      loginData.error = {
        headline: "Failed to login",
        text: "Cannot contact server",
      };
    }

    return loginData;
  }
};

export const logout = async () => {
  let refresh_token: string | null = localStorage.getItem("refreshToken");
  return axios
    .post(
      API_URL + "auth/logout",
      {
        refresh_token,
      },
      { headers: authHeader() }
    )
    .then((response) => {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      localStorage.removeItem("loginTime");
      return response.data;
    })
    .catch((error) => {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      localStorage.removeItem("loginTime");
      return error.response;
    });
};

const refresh = async () => {
  let refresh: string | null = localStorage.getItem("refreshToken");
  return axios
    .post(
      API_URL + "auth/refresh",
      {
        refresh,
      },
      { headers: authHeader() }
    )
    .then((response) => {
      if (response.data.access) {
        localStorage.setItem("accessToken", response.data.access);
        return true;
      }
      return false;
    })
    .catch((error) => {
      console.log(error);
      return false;
    });
};

//   async resetPassword(user_name: string) {
//     return axios
//       .post( API_URL + "auth/reset", {
//         user_name
//       }, {headers: authHeader()})
//   }

//   async getUserFromResetToken(reset_token: string) {
//     return axios.get(API_URL + "auth/change/" + reset_token);
//   }

//   async setNewPassword(reset_token: string, new_password: string) {
//     return axios.post(API_URL + "auth/change/" + reset_token, {
//       new_password
//     });
//   }
