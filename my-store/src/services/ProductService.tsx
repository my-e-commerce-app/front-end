import axios from "axios";
import { ErrorInterface } from "../interfaces/interfaces";

import authHeader from "./AuthHeader";

const API_URL = "http://localhost:5000/";

interface Product {
  name: string;
  type: string;
  id: number;
}

interface ProductsData {
  products?: Array<Product>;
  success: boolean;
  error?: ErrorInterface;
}

export const getProducts = async (): Promise<ProductsData> => {
  const returnData: ProductsData = {
    success: false,
  };
  try {
    const response = await axios.get(API_URL + "products", {
      headers: authHeader(),
    });
    if (response.data.products) {
      returnData.success = true;
      returnData.products = response.data.products;
      return returnData;
    }
    returnData.error = {
      headline: "Failed to login",
      text: "Failed to login",
    };

    return returnData;
  } catch (e: any) {
    const axiosError: AxiosError = e;
    console.log(axiosError);
    if (axiosError.response) {
      returnData.error = {
        headline: "Failed to login",
        text: "Failed to login, make sure user and / or password is correct",
      };
    } else {
      returnData.error = {
        headline: "Failed to login",
        text: "Cannot contact server",
      };
    }

    return returnData;
  }
};
