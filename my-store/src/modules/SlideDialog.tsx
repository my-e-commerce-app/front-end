import { useState, Fragment, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";

interface Props {
  open: boolean;
  close: () => void;
}

const SlideDialog: React.FC<Props> = ({ open, close }) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);
  return (
    <Transition show={isOpen} as={Fragment}>
      <Dialog
        open={isOpen}
        onClose={() => {
          setIsOpen(false);
          close();
        }}
        className="fixed top-0 right-0 z-50"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/30" />
        </Transition.Child>
        <Transition.Child
          as={Fragment}
          enterFrom="opacity-0 -translate-y-full"
          enterTo="opacity-100 translate-y-0"
          leave="transition ease duration-1000 transform"
          leaveFrom="opacity-100 translate-y-0"
          leaveTo="opacity-0 -translate-y-full"
        >
          <Dialog.Panel className="w-36 h-screen max-w-sm rounded bg-white">
            <Dialog.Title>Complete your order</Dialog.Title>

            <p>Your order is all ready!</p>
            <button></button>
          </Dialog.Panel>
        </Transition.Child>
      </Dialog>
    </Transition>
  );
};

export default SlideDialog;
