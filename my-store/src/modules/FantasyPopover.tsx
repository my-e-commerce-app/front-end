import { Popover, Transition } from "@headlessui/react";
import { ReactNode, useState } from "react";
import { usePopper } from "react-popper";
import clsx from "clsx";

interface Props {
  button: ReactNode;
  children: ReactNode;
  className?: string;
}

const FantasyPopover: React.FC<Props> = ({ button, children, className }) => {
  const [referenceElement, setReferenceElement] = useState();
  const [popperElement, setPopperElement] = useState();
  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [{ name: "offset", options: { offset: [0, 18] } }],
  });

  return (
    <Popover className={clsx("", className)}>
      <Popover.Button
        ref={setReferenceElement}
        className={"focus:outline-none"}
      >
        {button}
      </Popover.Button>

      <Transition
        enter="transition duration-100 ease-out"
        enterFrom="transform scale-100 opacity-0"
        enterTo="transform scale-100 opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform scale-100 opacity-100"
        leaveTo="transform scale-95 opacity-0"
      >
        <Popover.Panel
          ref={setPopperElement}
          style={styles.popper}
          {...attributes.popper}
        >
          {children}
        </Popover.Panel>
      </Transition>
    </Popover>
  );
};

export default FantasyPopover;
