const UserCard: React.FC = () => {
  return (
    <div className="flex flex-col justify-center items-center">
      <div className="h-0 w-0 border-x-8 border-b-8 border-x-transparent border-gray-300" />
      <div className="bg-gray-300 w-44 h-64 rounded-3xl"></div>
    </div>
  );
};

export default UserCard;
