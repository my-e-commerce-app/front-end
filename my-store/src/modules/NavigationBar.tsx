import { useState } from "react";

import ImageButton from "../Components/ImageButton";
import TextButton from "../Components/TextButton";
import FantasyPopover from "./FantasyPopover";
import ShoppingCardCard from "./ShoppingCartCard";
import SlideDialog from "./SlideDialog";

export const NavigationBar: React.FC = () => {
  const [openUserDialog, setOpenUserDialog] = useState(false);

  return (
    <div className="w-[98%] h-20 bg-purple-600 ml-auto mr-auto mt-2 mb-auto rounded-3xl flex items-center">
      <FantasyPopover
        button={
          <ImageButton imageSrc="/images/wagon.png" title="Shopping cart" />
        }
        children={<ShoppingCardCard />}
        className="ml-auto"
      />
      <ImageButton
        imageSrc="/images/adventurer.png"
        title="Profile"
        className="border-black rounded-full mr-10"
        width={50}
        onClick={() => {
          setOpenUserDialog(true);
        }}
      />
      <SlideDialog
        open={openUserDialog}
        close={() => {
          setOpenUserDialog(false);
        }}
      />
    </div>
  );
};
