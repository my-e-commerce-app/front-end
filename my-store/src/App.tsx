import { BrowserRouter, Routes, Route } from "react-router-dom";

import { useContext } from "react";
import { AuthContext } from "./Contexts/Auth";
import { Login } from "./Pages/Login";
import { Main } from "./Pages/Main";

const App: React.FC = () => {
  const { loggedInState } = useContext(AuthContext);
  const { loggedIn } = loggedInState;
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={!loggedIn ? <Main /> : <Login />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
