export interface ErrorInterface {
  headline: string;
  text: string;
}
