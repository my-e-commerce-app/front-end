import clsx from "clsx";
import React, { ChangeEvent } from "react";

interface Props {
  value: string;
  style: "bold" | "normal";
  size?: "small" | "large";
}

const TextElement: React.FC<Props> = ({ value, style, size }) => {
  return (
    <p
      className={clsx(
        "",
        {
          "font-bold": style === "bold",
          "font-normal": style === "normal",
        },
        {
          "text-base": !size,
          "text-sm": size === "small",
          "text-xl": size === "large",
        }
      )}
    >
      {value}
    </p>
  );
};

export default TextElement;
