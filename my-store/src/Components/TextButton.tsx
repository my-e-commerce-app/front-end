import React from "react";
import clsx from "clsx";
import TextElement from "./TextElement";
import LoadingElement from "./LoadingElement";
import Button from "./Button";

interface Props {
  onClick?: (e?: any) => void;
  disabled?: boolean;
  color?: "green" | "red" | "blue";
  text: string;
  loading?: boolean;
  className?: string;
}

const TextButton: React.FC<Props> = ({
  onClick,
  disabled,
  color = "blue",
  text,
  loading,
  className,
}) => {
  return (
    <Button
      className={clsx(
        "inline-flex justify-center rounded-md border border-transparent px-4 py-2 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 relative",
        {
          "bg-blue-200 hover:bg-blue-300 focus-visible:ring-blue-500":
            color === "blue",
          "bg-green-200 hover:bg-green-300 focus-visible:ring-green-500":
            color === "green",
          "bg-red-200 hover:bg-red-300 focus-visible:ring-red-500":
            color === "red",
          "cursor-not-allowed": disabled,
          "cursor-pointer": !disabled,
        },
        className
      )}
      disabled={disabled}
      onClick={onClick}
      children={
        <div>
          {loading ? <LoadingElement /> : null}
          <TextElement style="bold" value={text} size="large" />
        </div>
      }
    />
  );
};

export default TextButton;
