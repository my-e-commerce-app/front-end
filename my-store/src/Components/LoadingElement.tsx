const LoadingElement: React.FC = () => {
  return (
    <div className="absolute top-0 left-0 bg-gray-500 bg-opacity-50 w-full h-full rounded-md z-20 flex flex-row justify-center items-center">
      <img src="/images/loading.gif" className="w-10 h-10" />
    </div>
  );
};

export default LoadingElement;
