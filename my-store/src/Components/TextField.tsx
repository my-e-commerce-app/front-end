import clsx from "clsx";
import React, { ChangeEvent } from "react";

interface Props {
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  value?: string;
  placeholder?: string;
  disabled?: boolean;
  state?: boolean;
  type?: "password";
}

const TextField: React.FC<Props> = ({
  onChange,
  value,
  placeholder,
  disabled,
  state,
  type,
}) => {
  return (
    <input
      onChange={onChange}
      disabled={disabled}
      className={clsx(
        "shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline text-lg",
        { "border-red-500": !state }
      )}
      type={clsx("", {
        text: !type,
        password: type === "password",
      })}
      placeholder={placeholder}
      value={value}
    />
  );
};

export default TextField;
