import React from "react";
import clsx from "clsx";
import Button from "./Button";

interface Props {
  onClick?: (e?: any) => void;
  disabled?: boolean;
  imageSrc: string;
  loading?: boolean;
  className?: string;
  title?: string;
  width?: number;
}

const ImageButton: React.FC<Props> = ({
  onClick,
  disabled,
  imageSrc,
  className,
  title,
  width,
}) => {
  return (
    <Button
      className={clsx(
        "inline-flex justify-center rounded-md border border-transparent px-4 py-2 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 relative",
        className
      )}
      disabled={disabled}
      onClick={onClick}
      children={
        <div>
          <img src={imageSrc} title={title} width={width ? width : 75} />
        </div>
      }
    />
  );
};

export default ImageButton;
