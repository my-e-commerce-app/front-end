import React from "react";
import clsx from "clsx";

interface Props {
  onClick?: (e?: any) => void;
  disabled?: boolean;
  children: React.ReactNode;
  className?: string;
}

const Button: React.FC<Props> = ({
  onClick,
  disabled,
  className,
  children,
}) => {
  return (
    <button
      type="button"
      disabled={disabled}
      className={clsx(className)}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
